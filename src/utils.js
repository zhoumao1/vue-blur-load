/**
 * 绑定 after-leave 事件在 Vue 的子实例中, 确保动画结束后可以执行
 *
 * @param {Vue} instance Vue instance.
 * @param {Function} callback callback of after-leave event
 * @param {Number} speed the speed of transition, default value is 300ms
 * @param {Boolean} once weather bind after-leave once. default value is false.
 */
export const afterLeave = (instance, callback, speed = 300, once = false) => {
	if (!instance || !callback) throw new Error('instance & callback is required');
	let called = false;
	const afterLeaveCallback = function () {
		if (called) return;
		called = true;
		if (callback) {
			callback.apply(null, arguments);
		}
	};
	if (once) {
		instance.$once('after-leave', afterLeaveCallback);
	} else {
		instance.$on('after-leave', afterLeaveCallback);
	}
	setTimeout(() => {
		afterLeaveCallback();
	}, speed + 100);
};


let lockCount = 0;

export const lockClick =(lock, parent) =>{
	lockCount = +lockCount
	const element = parent || document.body
	if (lock) {
		if (lockCount <= 0) {
			element.classList.add('unclickable');
		}

		lockCount++;
	} else {
		lockCount--;

		if (lockCount <= 0) {
			element.classList.remove('unclickable');
		}
	}
}

export const on = (function() {
	if (document.addEventListener) {
		return function(element, event, handler) {
			if (element && event && handler) {
				element.addEventListener(event, handler, false);
			}
		};
	} else {
		return function(element, event, handler) {
			if (element && event && handler) {
				element.attachEvent('on' + event, handler);
			}
		};
	}
})();

export const off = (function() {
	if (document.removeEventListener) {
		return function(element, event, handler) {
			if (element && event) {
				element.removeEventListener(event, handler, false);
			}
		};
	} else {
		return function(element, event, handler) {
			if (element && event) {
				element.detachEvent('on' + event, handler);
			}
		};
	}
})();

export const once = function(el, event, fn) {
	var listener = function() {
		if (fn) {
			fn.apply(this, arguments);
		}
		off(el, event, listener);
	};
	on(el, event, listener);
};

