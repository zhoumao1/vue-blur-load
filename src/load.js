import Vue from 'vue'
import __components_loading from './components/loading'
import { afterLeave, lockClick, once, off } from './utils';
import { deepMerge } from './tools';

const isServer = Vue.prototype.$isServer
const LoadConstructor = Vue.extend(__components_loading)

let defaultOptions = {
	exist: true,
	content: '加载中',
	color: getComputedStyle(document.body).getPropertyValue('--theme') || '#EB8E45',
	small: false,
	mini: false,
	size: '',
	showLoadIcon: true,
	// onClose: null,
	// onOpened: null,
	duration: 2000,
};
let queue = []
let multiple = true
let delayTimer = null

function createLoadInstance() {
	/* istanbul ignore if */

	if (!queue.length || multiple) {
		let newLoad = new (Vue.extend(LoadConstructor))({
			el: document.createElement('div')
		});
		newLoad.$on('input', function (value) {
			newLoad.value = value;
		});
		newLoad.$once('click', function (value) {
			newLoad.value = value;
		});
		queue.push(newLoad);
		// console.log(queue, '创建')
	}

	return queue[queue.length - 1];
}

const Load = (options) => {
	if (options === void 0) {
		options = {};
	}
	let parent = options.el || document.body
	let loadInstance = createLoadInstance()
	const _this = loadInstance

	if (loadInstance.exist) {
		// console.log('已存在')
		// l.updateZIndex();
	}
	options = { ...defaultOptions, ...options }

	if(options.size){
		options.small = options.size.includes('small')
		options.mini = options.size.includes('mini')
	}

	Object.assign(loadInstance, options)
	parent.appendChild(loadInstance.$el)

	loadInstance.showLoading = true
	options.close = onClose.bind(_this, loadInstance);
	// options.success = onSuccess.bind(this, content)
	options.success = onSuccess;
	if (options.small || options.mini) {
		lockClick(true, parent)
	}
	if (options.el) {
		loadInstance.$el.style.position = 'absolute'
	} else {
		loadInstance.$el.style.position = ''
	}
	for (const defaultOptionsKey in options) {
		loadInstance[defaultOptionsKey] = options[defaultOptionsKey]
	}

	return loadInstance;
}
/**
 * 关闭加载
 * @param all  {boolean=}  是否关闭全部
 */
Load.close = function (all) {
	if (queue.length) {
		if (all) {
			queue.forEach(function (load) {
				load.close();
			});
			queue = [];
		} else if (!multiple) {
			queue[0].close();
		} else {
			queue.shift()
			.close();
			// console.log(queue.shift())
		}
	}
};
Load.show = (options) => {
	if (!options) {
		options = deepMerge(defaultOptions)
		return Load({ ...options })
	}
	if (typeof options === 'string') {
		options = { content: options }
		return Load({ ...options })
	} else if (!options.el) {
		return Load({ ...options })
	} else if (typeof options.el === 'object') {
		if (!options.el.tagName) throw 'options.el type is Element or Document'
		options.el.style.position = 'relative'
		return Load({ ...options })
	} else {
		return new Promise((resolve) => {
			setTimeout(() => {
				if (options && options.el) {
					document.querySelector(options.el).style.position = 'relative'
					options.el && options.el.includes('.') ? options.el_class = options.el : options.el_id = options.el
					options.el = document.querySelector(options.el)
					// .appendChild(document.createElement('div'))
				}
				options = { ...options }
				// return Load({ ...options })
				return resolve(Load({ ...options }))
			})
		})
	}
}
Load.message = (options) => {
	if (!options) {
		options = deepMerge(defaultOptions)
		options.content = ''
	}else if (typeof options !== 'object') {
		options = Object.assign({}, deepMerge(defaultOptions),{ content: options })
	}else {
		options = Object.assign({}, deepMerge(defaultOptions),options)
	}
	options.size = 'mini'
	options.showLoadIcon = false
	delayTime(options.duration).then(() =>{
		Load.close()
	})
	return Load({ ...options })
}

function onClose(loadInstance, after_close) {
	loadInstance.exist = false;

	// if (options.onClose) {
	// 	options.onClose();
	// }
	if (multiple && !isServer) {
		// eslint-disable-next-line no-unused-vars
		afterLeave(loadInstance, _ => {
			// const target = this.el || document.body
			after_close && typeof after_close === 'function' && after_close()
			removeTemplate(loadInstance)
			queue = queue.filter(function (item) {
				// 可能有问题 原: item !== this;
				return item._uid !== loadInstance._uid;
			});
		}, 100);
		off(this.$el, 'click', this.close);
		clearTimeout(delayTimer)
		lockClick(false, loadInstance.el)
		this.showLoading = false;
	}
}

function onSuccess(content, after_success) {
	if (typeof content === 'function') {
		after_success = content
		this.content = '处理成功'
	} else {
		this.content = content || '处理成功'
	}
	this.showLoadIcon = false
	this.mini = true
	this.color = '#2ea44f'
	delayTime(/*200000*/).then(() =>{
		this.close(after_success)
	})
	once(this.$el, 'click', () =>{
		this.close(after_success)
	})
}

/**
 * 延迟
 * @param time {number=} 毫秒级
 * @returns {Promise<unknown>}
 */
function delayTime(time = 800) {
	return new Promise(resolve => {
		delayTimer = setTimeout(() => { resolve() }, time);
	});
}

function removeTemplate(template) {
	if (template.$el && template.$el.parentNode) {
		template.$el.parentElement.style.position = ''
		template.$el.parentNode.removeChild(template.$el);
	}
	template.$destroy();
	// delete loadHash[key]
}

window.addEventListener('popstate', () => {
	Load.close(true)
})
export default Load

