
/**
 * 深度合并对象, 并不是引用指针
 * @param source
 * @return {null}
 */
export const deepMerge = (...source) =>{
	const result = Object.create(null)
	const resultArr = []
	const deepCopy = (target) =>{

		let copyed_objs = [];//此数组解决了循环引用和相同引用的问题，它存放已经递归到的目标对象
		function _deepCopy(target){
			if((typeof target !== 'object')||!target){return target;}
			if(!['[object Object]', '[object Array]'].includes(Object.prototype.toString.call(target)))return target
			for(let i= 0 ;i<copyed_objs.length;i++){
				if(copyed_objs[i].target === target){
					return copyed_objs[i].copyTarget;
				}
			}
			let obj = {};
			if(Array.isArray(target)){
				obj = [];//处理target是数组的情况
			}
			copyed_objs.push({target:target,copyTarget:obj})
			Object.keys(target).forEach(key=>{
				if(obj[key]){ return;}
				obj[key] = _deepCopy(target[key]);
			});
			return obj;
		}
		return _deepCopy(target);
	}
	source.forEach(item =>{
		if(Array.isArray(item)){
			item && resultArr.push(deepCopy(item))
		}else {
			item && Object.assign(result, deepCopy(item))
		}
	})

	return resultArr.length ? resultArr.flat(Infinity):result
}

function isDef(val) {
	return val !== undefined && val !== null;
}
export function deepClone(obj) {

	if (!(0, isDef)(obj)) {
		return obj;
	}

	if (Array.isArray(obj)) {
		return obj.map(function (item) {
			return deepClone(item);
		});
	}

	if (['[object Object]', '[object Array]'].includes(Object.prototype.toString.call(obj))) {
		var to = {};
		Object.keys(obj).forEach(function (key) {
			to[key] = deepClone(obj[key]);
		});
		return to;
	}

	return obj;
}

/**
 * 判空
 * @param {*}  value
 * @return {boolean}
 */
export function isEmpty(value) {
	if (value === "") return true; //检验空字符串
	if (value === "null") return true; //检验字符串类型的null
	if (value === "undefined") return true; //检验字符串类型的 undefined
	if (!value && value !== 0 && value !=="") return true; //检验 undefined 和 null
	// eslint-disable-next-line no-prototype-builtins
	if (Array.prototype.isPrototypeOf(value) && value.length === 0 ) return true; //检验空数组
	// eslint-disable-next-line no-prototype-builtins
	if (Object.prototype.isPrototypeOf(value) && Object.keys(value).length === 0 ) return true;  //检验空对象
	return false;
}

/**
 * 获取对象深层属性值
 * @param {Object}      obj            对象
 * @param {string}      path           路径
 * @example // path
 * var obj = { name: 'xxx', address: { cities: [{ city: '山西' }] } }
 * getObjectValue(obj, 'address.cities[0].city')
 * @param {string}      [def_value = '']      默认展示值
 * @return {*}
 */
export function getObjectValue (obj, path, def_value = '') {
	let result = obj, i = 0;
	const paths = path.replace(/\[(\d+)]/g, '.$1').split('.')
	while (i < paths.length) {
		result = Object(result)[paths[i]];
		if (result === undefined) {
			return def_value;
		}
		i++;
	}
	return result;
}

/**
 * 根据 path 设置 object
 * @param {Object|Array}   o      object | Array
 * @param {string}         path     路径
 * @param {*}              [value]
 * @param {function}       [$set]   Vue.set
 */
export function setObjectValue (o, path, value = '', $set) {
	let result = o, i = -1;
	//新增这一行————————————————————————————
	const paths = path.replace(/\[(\d+)\]/g, '.$1').split('.')
	let length = paths.length
	let lastIndex = length - 1
	while (result !== null && ++i < length) {
		const key = paths[i]
		let newValue = value

		if(i !== lastIndex){
			const objValue = result[key]
			newValue = typeof objValue === 'object' ? objValue: /^[0-9]+$/.test(paths[i+1]) ? []:{}
		}
		if($set){
			$set(result, key, newValue)
		}else {
			result[key] = newValue
		}
		result = result[key]
	}
}

/**
 * 克隆/扩展对象 只会克隆 object 中存在 key 的值
 * @param {Object}   object   目标
 * @param {Object}   source   资源
 * @param {Object}   o      额外资源
 * @example
 * var submitObj = { name: null, age: null }
 * var formData = { id: 'xxx', name: 'test', age: 30 }
 * cloneObj(submitObj, formData) ==> submitObj = { name: 'test', age: 30 }
 */
export function cloneObj(object, source, ...o){
	for (const sourceKey in source) {
		if(hasKey(object, sourceKey)){
			object[sourceKey] = source[sourceKey]
		}
	}
	Object.assign(object, ...o)
}


/**
 * 判断一个对象是否存在key，如果传入第二个参数key，则是判断这个obj对象是否存在key这个属性
 * 如果没有传入key这个参数，则判断obj对象是否有键值对
 */
export function hasKey (obj, key) {
	if (key) return key in obj
	else {
		let keysArr = Object.keys(obj)
		return keysArr.length
	}
}

/**
 * 根据 key(id) 求并集
 * @param {string}   flag_key
 * @param {Array<Object>}    arr1
 * @param {Array<Object>}    arr2
 * @return {Array}
 */
export function getUnionByKey (flag_key, arr1,arr2){
	const arr2Id = (arr) => {
		return arr && arr.map(item=>item[flag_key] ? item[flag_key] : item);
	}
	const id2Arr = (idArr, arr) => {
		return idArr && idArr.map(id => {
			let curr = id;
			arr && arr.forEach(item => item[flag_key] === id && (curr = item));
			return curr;
		})
	}
	let mergeArr = arr1 && arr2 && arr1.concat(arr2);
	let _arr1 = arr2Id(arr1);
	let _arr2 = arr2Id(arr2);
	let idArr = [...new Set(_arr1.concat(_arr2))];
	return id2Arr(idArr,mergeArr);
}
