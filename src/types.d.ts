import Vue, { PluginObject } from "vue";

type LoadSize = 'default' | 'small' | 'mini';


export type LoadOptions = {
	/** 挂载到指定元素中, 注: 此时返回 Promise */
	el?: HTMLElement | Document | string;
	/** 展示时长(ms) */
	duration?: number;

	/** 加载文案 */
	content?: string;
	/** 主题色 */
	color?: string;
	/** 是否为小卡片类型的加载 */
	size?: LoadSize;
};

export type MessageOptions = {
	/** 展示时长(ms) */
	duration?: number;

	/** 加载文案 */
	content?: string;

	/** 主题色 */
	color?: string;
};

/** 加载 组件 */
export interface LoadComponent extends Vue {
	// 加载文案
	content: '加载中';
	/** 关闭加载的实例 */
	close (after_close?: Function): void;
	/** 操作处理成功 */
	// @ts-ignore
	success (content?: '处理成功', after_success?: Function): void;
	success (content: '处理成功'): void;
	success (after_success?: Function): void;

}

// loading 的状态, 显示还是隐藏
export interface LoadStatus {
	/** 文案提示 */
	message (content?: string | MessageOptions);

	/** 显示 loading */
	show (content?: string | LoadOptions): LoadComponent | Promise<LoadComponent>;

	// /** 显示 loading */
	// show (options: LoadOptions): LoadComponent | Promise<LoadComponent>;

	/** 关闭 loading is_all: 是否关闭所有 */
	close (is_all?: boolean): LoadComponent;
}

declare module 'vue/types/vue' {
	interface Vue {
		$Load: LoadStatus
	}
}
export const $Load: LoadStatus;
